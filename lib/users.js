const users = [
    {
        name: 'Angela',
        path: '/images/angela.jpg'
    },
    {
        name: 'Mark',
        path: '/images/mark.jpg'
    },
    {
        name: 'Phillip',
        path: '/images/phillip.jpg'
    },
    {
        name: 'Bob Dilan',
        path: '/images/bob.jpg'
    },
    {
        name: 'Anna',
        path: '/images/anna.jpeg'
    },
    {
        name: 'Santa',
        path: '/images/santa.jpg'
    },
    {
        name: 'Alien',
        path: '/images/alien.jpg'
    },
]

module.exports = users;

